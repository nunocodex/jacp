<?php

return [
    'job' => 'Job',
    'queues' => 'Queues',

    'result' => 'Result',

    // Params
    'params' => [
        'jk_device_codename' => 'Device',
        'jk_make_clean' => 'Clean',
        'jk_repo_sync' => 'Sync'
    ],

    'duration' => 'Duration',
    'created_at' => 'Created At',

    'actions' => 'Actions',

    'build' => [
        'start' => 'Start new build',
        'cancel' => 'Cancel',
        'remove' => 'Remove'
    ],

    'messages' => [
        'success' => [
            'start' => 'Build successfully added in queue!',
            'cancel' => 'Build successfully cancelled...',
            'remove' => 'Build queue successfully removed'
        ],

        'error' => [
            'start' => 'Build not added in queue',
            'cancel' => 'Build not cancelled',
            'remove' => 'Build queue not removed'
        ]
    ]
];

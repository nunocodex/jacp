@extends('layouts.app')

@section('title', 'Create new build - ')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('jenkins.build.start') }} <small>{{ $job }}</small></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('builds.start', ['job' => $job]) }}">
                            @csrf

                            @foreach($params as $p_key => $p_value)
                                <div class="form-group row">
                                    <label for="{{ $p_key }}" class="col-md-4 col-form-label text-md-right">{{ __('jenkins.params.' . $p_key) }}</label>

                                    <div class="col-md-6">
                                        @if(is_array($p_value))
                                            <select name="{{ $p_key }}" id="{{ $p_key }}" class="form-control">
                                                @foreach($p_value as $v)
                                                    <option value="{{ $v }}">{{ ucfirst($v) }}</option>
                                                @endforeach
                                            </select>
                                        @elseif (empty($p_value))
                                            <select name="{{ $p_key }}" id="{{ $p_key }}" class="form-control">
                                                @foreach(Auth::user()->devices as $v)
                                                    <option value="{{ $v }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" name="{{ $p_key }}" class="form-control" />
                                        @endif

                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('jenkins.build.start') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title', 'Builds - ')

@section('content')
    <div class="container">
        <?php use Utarwyn\Jenkins\Entity\Project; /** @var Project $job */ ?>
        @foreach($jobs as $job)
            <div class="row justify-content-center mb-4">
                <div class="col-md-12">
                    <div class="card">
                        <h3 class="card-header">
                            {{ __('jenkins.job') }}: <small>{{ $job->getName() }}</small>

                            @can('start-build', [$job->getName()])
                                <a href="{{ route('builds.create', ['job' => $job->getName()]) }}" class="btn btn-primary btn-sm float-right">
                                    {{ __('jenkins.build.start') }}
                                </a>
                            @endcan
                        </h3>

                        <div class="card-body">
                            <table id="{{ strtolower($job->getName()) }}" class="table table-bordered table-sm jobs" style="width: 100%">
                                <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>{{ __('jenkins.result') }}</th>

                                    @foreach($job->getParametersDefinition() as $p_key => $p_value)
                                        <th>{{ __('jenkins.params.' . $p_key) }}</th>
                                    @endforeach

                                    <th>{{ __('jenkins.duration') }}</th>
                                    <th>{{ __('jenkins.created_at') }}</th>

                                    @can('build')
                                        <th>{{ __('jenkins.actions') }}</th>
                                    @endcan

                                </tr>
                                </thead>
                                <tbody>
                                <?php use JenkinsKhan\Jenkins\Build; /** @var Build $build */ ?>
                                @foreach($job->getBuilds() as $build)
                                    <tr class="table-{{ strtolower($build->getResult() ?? 'building') }}">
                                        <td>#{{ $build->getNumber() }}</td>
                                        <td>{{ $build->getResult() }}</td>

                                        @foreach($build->getInputParameters() as $p_key => $p_value)
                                            <td>{{ $p_value }}</td>
                                        @endforeach

                                        <td>{{ $build->getDuration() }}</td>
                                        <td>{{ $build->getTimestamp() }}</td>

                                        @can('build')
                                            <td>
                                                @can('manage-build', [$build->getInputParameters()])
                                                    @if($build->isRunning())
                                                        <a href="{{ route('builds.cancel', ['job' => $job->getName(), 'device' => $build->getInputParameters()['jk_device_codename'], 'build' => $build->getNumber()]) }}" class="btn btn-primary btn-sm">{{ __('jenkins.build.cancel') }}</a>
                                                    @endif
                                                @endcan
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>{{ __('jenkins.result') }}</th>

                                    @foreach($job->getParametersDefinition() as $p_key => $p_value)
                                        <th>{{ __('jenkins.params.' . $p_key) }}</th>
                                    @endforeach

                                    <th>{{ __('jenkins.duration') }}</th>
                                    <th>{{ __('jenkins.created_at') }}</th>

                                    @can('build')
                                        <th>{{ __('jenkins.actions') }}</th>
                                    @endcan
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    @if($queues)
        <div class="container">
            @foreach($queues as $queue_name => $queue)
                <div class="row justify-content-center mb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">{{ __('jenkins.queues') }} <small>{{ $queue_name }}</small></div>

                            <div class="card-body">
                                <table id="queue-{{ strtolower($queue_name) }}" class="table table-bordered" style="width: 100%">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>ID</th>

                                        @foreach($queue[0]->params as $p_key => $p_value)
                                            <th>{{ __('jenkins.params.' . $p_key) }}</th>
                                        @endforeach

                                        @can('build')
                                            <th>{{ __('jenkins.actions') }}</th>
                                        @endcan

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($queue as $_queue)
                                        <tr>
                                            <td>#{{ $_queue->id }}</td>

                                            @foreach($_queue->params as $p_key => $p_value)
                                                <td>{{ $p_value }}</td>
                                            @endforeach

                                            @can('build')
                                                <td>
                                                    @can('manage-build', [$_queue->params])
                                                        <a href="{{ route('builds.remove', ['id' => $_queue->id]) }}" class="btn btn-primary btn-sm">{{ __('jenkins.build.remove') }}</a>
                                                    @endcan
                                                </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot class="thead-dark">
                                    <tr>
                                        <th>ID</th>

                                        @foreach($queue[0]->params as $p_key => $p_value)
                                            <th>{{ __('jenkins.params.' . $p_key) }}</th>
                                        @endforeach

                                        @can('build')
                                            <th>{{ __('jenkins.actions') }}</th>
                                        @endcan
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endsection

@section('stylesheets')
    <!-- DataTables -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('javascripts')
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.table.jobs').DataTable({
                'order': [[7, 'desc']]
            });
        });
    </script>
@endsection

<?php

return [
    'base_url' => env('JENKINS_BASE_URL'),
    'url' => env('JENKINS_URL'),
    'username' => env('JENKINS_USERNAME'),
    'password' => env('JENKINS_PASSWORD'),

    'jobs' => [
        'aosup-ten'
    ],

    'users' => [
        [
            'name'      => 'NunoCodex',
            'email'     => 'nunocodex@gmail.com',
            'password'  => 'R1ccard0',
            'role'      => 'admin',
            'jobs'      => [
                'arrow-ten' => [
                    'jk_device_codename'   => ['lavender', 'ginkgo']
                ]
            ]
        ],
        [
            'name'      => 'DarkDroidDev',
            'email'     => 'darkdroid.dev@gmail.com',
            'password'  => 'darkdroid',
            'role'      => 'maintainer',
            'jobs'      => [
                'yodita-ten' => [
                    'jk_device_codename'   => ['lavender']
                ]
            ]
        ]
    ]
];

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Block register and reset password
Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/builds', 'BuildsController@index')->name('builds');
Route::get('/builds/{job}', 'BuildsController@create')->middleware('can:start-build,job')->name('builds.create');
Route::post('/builds/{job}', 'BuildsController@start')->middleware('can:start-build,job')->name('builds.start');
Route::get('/builds/{job}/{device}/{build}/cancel', 'BuildsController@cancel')->middleware('can:manage-build,job,device')->name('builds.cancel');
Route::get('/builds/{id}/remove', 'BuildsController@remove')->name('builds.remove');

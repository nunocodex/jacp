<?php

namespace App\Providers;

use App\Auth\User;
use App\Extensions\CollectionUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('collection', function ($app, array $config) {
            return new CollectionUserProvider(config('jenkins.users'));
        });

        Gate::define('build', function (User $user) {
            return ($user->role === 'admin' or $user->role === 'maintainer');
        });

        Gate::define('start-build', function (User $user, string $job) {
            if ($user->role === 'admin') {
                return true;
            } elseif ($user->role === 'maintainer' and in_array($job, $user->jobs)) {
                return true;
            }

            return false;
        });

        Gate::define('manage-build', function (User $user, array $params) {
            if ($user->role === 'admin') {
                return true;
            } elseif (
                $user->role === 'maintainer' and
                isset($params[config('jenkins.device_field_name')]) and
                in_array($params[config('jenkins.device_field_name')], $user->devices)
            ) {
                return true;
            }

            return false;
        });
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use JenkinsApi\Jenkins;

/**
 * Class JenkinsServiceProvider
 *
 * @package App\Providers
 */
class JenkinsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Jenkins::class, function ($app) {
            return new Jenkins(
                config('jenkins.url'),
                config('jenkins.username'),
                config('jenkins.password')
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

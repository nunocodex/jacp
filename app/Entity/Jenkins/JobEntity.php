<?php

namespace App\Entity\Jenkins;

class JobEntity extends AbstractEntity
{
    /**
     * @return array
     */
    public function getParametersDefinition()
    {
        $parameters = array();

        foreach ($this->_data->actions as $action) {
            if (!property_exists($action, 'parameterDefinitions')) {
                continue;
            }

            foreach ($action->parameterDefinitions as $parameterDefinition) {
                $default = property_exists($parameterDefinition, 'defaultParameterValue') ? $parameterDefinition->defaultParameterValue->value : null;
                $description = property_exists($parameterDefinition, 'description') ? $parameterDefinition->description : null;
                $choices = property_exists($parameterDefinition, 'choices') ? $parameterDefinition->choices : null;

                $parameters[$parameterDefinition->name] = array('default' => $default, 'choices' => $choices, 'description' => $description,);
            }
        }

        return $parameters;
    }

    public function getBuilds()
    {
        return $this->_data->builds;
    }

    /**
     * @return boolean
     */
    public function isBuildable(): bool
    {
        return $this->_data->buildable;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->_data->name;
    }
}

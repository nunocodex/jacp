<?php
/**
 * @package    Atlas
 * @author     Christopher Biel <christopher.biel@jungheinrich.de>
 * @copyright  2013 Jungheinrich AG
 * @license    Proprietary license
 * @version    $Id$
 */

namespace App\Entity\Jenkins;

use stdClass;

/**
 * Abstract class for all items that can be access via jenkins api
 *
 * @package    JenkinsApi
 * @author     Christopher Biel <christopher.biel@jungheinrich.de>
 * @version    $Id$
 */
abstract class AbstractEntity
{
    /**
     * @var stdClass
     */
    protected $_data;

    public function __construct(stdClass $data)
    {
        $this->_data = $data;
    }

    /**
     * @param string $propertyName
     *
     * @return string|int|null|stdClass
     */
    public function get($propertyName)
    {
        if ($this->_data instanceof stdClass && property_exists($this->_data, $propertyName)) {
            return $this->_data->$propertyName;
        }
        return null;
    }

    public function __get($property)
    {
        return $this->get($property);
    }

    public function __isset($property)
    {
        return array_key_exists($property, $this->_data);
    }

    public function __call($name, $args = array())
    {
        if(strlen($name) > 3 && substr($name, 0, 3) === 'get') {
            return $this->get(lcfirst(substr($name, 3)));
        }
    }
}

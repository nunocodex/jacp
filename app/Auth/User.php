<?php

namespace App\Auth;

use ArrayObject;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * App\Auth\User
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property array $devices
 * @property array $jobs
 */
class User extends ArrayObject implements Authenticatable
{
    /**
     * User constructor.
     * @param array $input
     * @param int $flags
     * @param string $iterator_class
     */
    public function __construct($input = array(), $flags = 2, $iterator_class = "ArrayIterator")
    {
        parent::__construct($input, $flags, $iterator_class);
    }


    /**
     * @return string
     */
    public function getAuthIdentifierName(): string
    {
        return 'email';
    }

    /**
     * @return string
     */
    public function getAuthIdentifier(): string
    {
        //dump($this); exit;
        return $this->{$this->getAuthIdentifierName()};
    }

    /**
     * @return string
     */
    public function getAuthPassword(): string
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }


    public static function all(): array
    {
        return [];
    }
}

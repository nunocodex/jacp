<?php

namespace App\Http\Controllers;

use App\Entity\Jenkins\JobEntity;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use JenkinsApi\Jenkins;
use stdClass;

/**
 * Class BuildsController
 *
 * @package App\Http\Controllers
 */
class BuildsController extends Controller
{
    /**
     * @param $actions
     * @return array
     */
    private function getInputParameters($actions): array
    {
        $parameters = array();
        if ($actions === null || $actions === array()) {
            return $parameters;
        }

        foreach ($actions as $action) {
            if (property_exists($action, 'parameters')) {
                foreach ($action->parameters as $parameter) {
                    if (property_exists($parameter, 'value')) {
                        $parameters[$parameter->name] = $parameter->value;
                    } elseif (property_exists($parameter, 'number') && property_exists($parameter, 'jobName')) {
                        $parameters[$parameter->name]['number'] = $parameter->number;
                        $parameters[$parameter->name]['jobName'] = $parameter->jobName;
                    }
                }
                break;
            }
        }

        return $parameters;
    }

    /**
     * @param Jenkins $jenkins
     * @return array
     */
    private function getJobs(Jenkins $jenkins): array
    {
        $projects = [];

        foreach ($jenkins->getJobs() as $job) {
            $project_url = sprintf('job/%s/api/json', $job->getName());
            $project_params = [
                'tree' => 'builds[actions[parameters[name,value]],name,result,url,building,number,duration,estimatedDuration,timestamp]'
            ];

            $project = $jenkins->get($project_url, 1, $project_params);

            $project->name = $job->getName();

            foreach ($project->builds as $build) {
                $build->status_class = $this->getBuildStyleClass($build->result);

                $build->timestamp = $this->getBuildDatetime($build->timestamp);
                $build->duration = $this->getBuildDatetime($build->duration);

                // If result is null still building
                if (null === $build->result) {
                    $build->result = __('BUILDING');
                    $build->duration = time() - $build->timestamp;
                }

                $build->parameters = $this->getInputParameters($build->actions);

                unset($build->actions, $build->_class);
            }

            $projects[] = $project;
        }

        return $projects;
    }

    /**
     * @param Jenkins $jenkins
     * @return array
     */
    private function getQueues(Jenkins $jenkins): array
    {
        $queues = [];

        foreach ($jenkins->getQueue()->getJobQueues() as $_queue) {
            $queue = new stdClass();

            $queue->id = $_queue->getId();
            $queue->name = $_queue->getJobName();
            $queue->params = $_queue->getInputParameters();

            $queues[$_queue->getJobName()][] = $queue;
        }

        return $queues;
    }

    /**
     * @param int $time
     * @return int
     */
    private function getBuildDatetime(int $time): int
    {
        return $time / 1000;
    }

    /**
     * @param string|null $result
     * @return string
     */
    private function getBuildStyleClass(?string $result): string
    {
        switch ($result) {
            case 'SUCCESS':
                $status_class = 'success';
                break;

            case 'ABORTED':
                $status_class = 'secondary';
                break;

            case 'FAILURE':
                $status_class = 'danger';
                break;

            case null:
                $status_class = 'info';
                break;

            default:
                $status_class = 'primary';
                break;
        }

        return $status_class;
    }

    /**
     * @param Jenkins $jenkins
     * @return Factory|View
     */
    public function index(Jenkins $jenkins)
    {
        $jobs = [];

        foreach (config('jenkins.jobs') as $job) {
            $job_url = sprintf('job/%s/api/json', $job);
            $job_params = [
                'tree' => 'name,builds[actions[parameters[name,value]],name,result,url,building,number,duration,estimatedDuration,timestamp]'
            ];

            $jobs[] = new JobEntity($jenkins->get($job_url, 1, $job_params));
        }

        dd($jobs);

        return view('builds.index', [
            'jobs' => $jobs,
            'queues' => [] //$jenkins->getQueue()->getJobQueues()
        ]);
    }

    /**
     * @param string $job
     * @return Application|Factory|RedirectResponse|Redirector|View
     */
    public function create(string $job)
    {
        return view('builds.create', [
            'job' => $job,
            'params' => $this->getParamsFromJob($job)
        ]);
    }

    /**
     * @param string $job
     * @param Request $request
     * @param Jenkins $jenkins
     * @return RedirectResponse|Redirector
     */
    public function start(string $job, Request $request, Jenkins $jenkins)
    {
        $result = $jenkins->getJob($job)->launch([
            'jk_device_codename'    => $request->input('device'),
            'jk_make_clean'         => $request->input('clean'),
            'jk_repo_sync'          => $request->input('sync')
        ]);

        if ($result) {
            $request->session()->flash('success', __('jenkins.messages.success.start'));
        } else {
            $request->session()->flash('fail', __('jenkins.messages.error.start'));
        }

        return redirect(route('builds'));
    }

    /**
     * @param string $job
     * @param string $device
     * @param int $build
     * @param Request $request
     * @param Jenkins $jenkins
     * @return RedirectResponse|Redirector
     */
    public function cancel(string $job, string $device, int $build, Request $request, Jenkins $jenkins)
    {
        $result = $jenkins->getBuild($job, $build)->stop();

        if ($result) {
            $request->session()->flash('success', __('jenkins.messages.success.cancel'));
        } else {
            $request->session()->flash('fail', __('jenkins.messages.error.cancel'));
        }

        return redirect(route('builds'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param Jenkins $jenkins
     * @return Application|RedirectResponse|Redirector
     */
    public function remove(int $id, Request $request, Jenkins $jenkins)
    {
        $project_url = sprintf('queue/item/%s/cancelQueue', $id);

        $result = $jenkins->post($project_url);

        if ($result) {
            $request->session()->flash('success', __('jenkins.messages.success.remove'));
        } else {
            $request->session()->flash('fail', __('jenkins.messages.error.remove'));
        }

        return redirect(route('builds'));
    }
}

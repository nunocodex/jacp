<?php


namespace App\Extensions;

use App\Auth\User;
use ArrayIterator;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Collection;

/**
 * Class CollectionUserProvider
 * @package App\Extensions
 */
class CollectionUserProvider extends Collection implements UserProvider
{
    /**
     * @param mixed $identifier
     * @return User|null
     */
    public function retrieveById($identifier): ?User
    {
        /** @var ArrayIterator $item */
        foreach ($this->getIterator() as $item) {
            $user = new User($item);

            if ($user->getAuthIdentifier() === $identifier) {
                return $user;
            }
        }

        return null;
    }

    public function retrieveByToken($identifier, $token)
    {
        throw new \BadMethodCallException('Method not allowed');
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        throw new \BadMethodCallException('Method not allowed');
    }

    /**
     * @param array $credentials
     * @return Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials): ?Authenticatable
    {
        /** @var ArrayIterator $item */
        foreach ($this->getIterator() as $item) {
            $user = new User($item);

            if ($this->validateCredentials($user, $credentials)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @param Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        return ($user->getAuthIdentifier() === $credentials['email'] && $user->getAuthPassword() === $credentials['password']);
    }

}
